package ru.inshakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @Nullable
    List<Task> removeAllTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
