package ru.inshakov.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.inshakov.tm.comparator.ComparatorByCreated;
import ru.inshakov.tm.comparator.ComparatorByDateStart;
import ru.inshakov.tm.comparator.ComparatorByName;
import ru.inshakov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

@Getter
@AllArgsConstructor
public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by date start", ComparatorByDateStart.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private String displayName;

    private Comparator comparator;

}
